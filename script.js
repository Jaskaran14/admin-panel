const body = document.querySelector("body"),
  sidebar = body.querySelector("nav"),
  // maskedDiv = body.querySelector(".masking"),
  sidebarBtn = body.querySelector(".sidebar-btn");

maskedDiv = document.createElement("div");
maskedDiv.classList.add("masking");

sidebarBtn.addEventListener("click", () => {
  sidebar.classList.toggle("close");
  // body.appendChild(maskedDiv);
  sidebar.insertBefore(maskedDiv, sidebar.firstChild);
});

// document.addEventListener("click", (event) => {
//   if (event.target != sidebarBtn && !sidebar.contains(event.target)) {
//     sidebar.classList.add("close");
//   }
// });

maskedDiv.addEventListener("click", (event) => {
  sidebar.classList.toggle("close");
  sidebar.removeChild(maskedDiv);
});
